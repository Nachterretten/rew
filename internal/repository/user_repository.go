package repository

import (
	"github.com/jmoiron/sqlx"
	"log"
	"rew/internal/models"

	_ "github.com/lib/pq"
)

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (u *UserRepository) CreateUser(user models.User) error {
	query := "INSERT INTO users (id, first_name, last_name, age, email, phone, address, city, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)"
	_, err := u.db.Exec(query, user.ID, user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country)
	if err != nil {
		log.Println("failed to create user: ", err)
	}

	return nil
}

func (u *UserRepository) GetUserList() ([]models.User, error) {
	query := "SELECT * FROM users"

	rows, err := u.db.Query(query)
	if err != nil {
		log.Println("failed to execute the query", err)
		return nil, err
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		if scanErr := rows.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Age, &user.Email, &user.Phone, &user.Address, &user.City, &user.Country); scanErr != nil {
			log.Println("failed to scan row", scanErr)
			continue
		}
		users = append(users, user)
	}

	if err := rows.Err(); err != nil {
		log.Println("error occurred while iterating rows", err)
		return nil, err
	}

	return users, nil
}

func (u *UserRepository) DeleteUser(id int) error {
	query := "DELETE FROM users WHERE id = $1"
	_, err := u.db.Exec(query, id)
	if err != nil {
		log.Println("failed to delete user: ", err)
		return err
	}
	return nil
}

func (u *UserRepository) UpdateUser(user models.User) error {
	query := `UPDATE users SET first_name = $1, last_name = $2, age = $3, email = $4, phone = $5, address = $6, city = $7, country = $8 WHERE id = $9`
	_, err := u.db.Exec(query, user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country, user.ID)
	if err != nil {
		log.Println("failed to update user: ", err)
		return err
	}

	return nil
}

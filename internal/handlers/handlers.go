package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rew/internal/models"
	"strconv"
)

func (s *APIServer) CreateUserHandler(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := s.userRepo.CreateUser(user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Status(http.StatusCreated)
}

func (s *APIServer) GetUserListHandler(c *gin.Context) {
	users, err := s.userRepo.GetUserList()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Failed to get users"})
		return
	}
	c.JSON(http.StatusOK, users)
}

func (s *APIServer) DeleteUserHandler(c *gin.Context) {
	id := c.Param("id")

	userID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	err = s.userRepo.DeleteUser(userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete user"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User deleted successfully"})
}

func (s *APIServer) UpdateUserHandler(c *gin.Context) {
	// Получаем идентификатор пользователя из параметров запроса
	id := c.Param("id")

	// Преобразуем идентификатор в целое число
	userID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	// Получаем данные пользователя из тела запроса
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Устанавливаем ID пользователя из параметров запроса
	user.ID = userID

	// Обновляем данные пользователя в базе данных
	err = s.userRepo.UpdateUser(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update user"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User updated successfully"})
}

package handlers

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"rew/internal/configs"
	"rew/internal/repository"
	"syscall"
	"time"
)

type APIServer struct {
	config   *configs.Config
	userRepo *repository.UserRepository
	configDB *configs.DB
}

func NewServer(config *configs.Config, userRepo *repository.UserRepository, cfgDB *configs.DB) *APIServer {
	return &APIServer{
		config:   config,
		userRepo: userRepo,
		configDB: cfgDB,
	}
}

func (s *APIServer) Run() {
	server := http.Server{
		Addr: s.config.HTTPAddr,
	}
	log.Printf("Listening on port %s", s.config.HTTPAddr)
	// создаем канал, который будет использоавться для сигнала завершения
	idConnClosed := make(chan struct{})
	// горутина, которая будет слушать сигналы прерываания или завершения
	go func() {
		sigint := make(chan os.Signal, 1)
		// os.Interrupt, syscall.SIGTERM - сигналы завершения программы
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			log.Fatalln(err)
		}
		close(idConnClosed)
	}()

	if err := server.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			log.Fatalln(err)
		}
	}
	<-idConnClosed
	log.Println("Server stopped")
}

func SetupRouter(s *APIServer) *gin.Engine {
	router := gin.Default()

	router.POST("/user", s.CreateUserHandler)
	router.GET("/users", s.GetUserListHandler)
	router.DELETE("/user/:id", s.DeleteUserHandler)
	router.PUT("/user/:id", s.UpdateUserHandler)
	return router
}

package configs

type Config struct {
	HTTPAddr string `json:"http_addr"`
}

func NewConfig() *Config {
	return &Config{
		HTTPAddr: ":8080",
	}
}

type DB struct {
	Driver   string
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
}

func NewDB() *DB {
	return &DB{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "root",
		Password: "qwerty",
		Port:     "5432",
		DBName:   "root",
	}
}

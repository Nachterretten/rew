package models

type User struct {
	ID        int    `db:"id"`
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
	Age       int    `db:"age"`
	Email     string `db:"email"`
	Phone     string `db:"phone"`
	Address   string `db:"address"`
	City      string `db:"city"`
	Country   string `db:"country"`
}

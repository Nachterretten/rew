package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"rew/internal/configs"

	_ "github.com/lib/pq"
)

func NewSqlDB(dbConf configs.DB) (*sqlx.DB, error) {
	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.DBName)

	db, err := sqlx.Open(dbConf.Driver, dsn)
	if err != nil {
		return nil, fmt.Errorf("failed to connect db")
	}

	/*	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("failed to chek connection")
	}*/

	return db, nil
}

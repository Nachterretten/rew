package main

import (
	"log"
	"rew/internal/configs"
	"rew/internal/db"
	"rew/internal/handlers"
	"rew/internal/repository"
)

func main() {
	// подключение к бд
	dbConfig := configs.NewDB()
	sqlDB, err := db.NewSqlDB(*dbConfig)
	if err != nil {
		log.Fatal(err)
	}
	defer sqlDB.Close()

	// создаем репу
	userRepo := repository.NewUserRepository(sqlDB)

	// создаем сервер
	cfg := configs.NewConfig()
	apiServer := handlers.NewServer(cfg, userRepo, dbConfig)

	// запускаем роутер
	router := handlers.SetupRouter(apiServer)

	err = router.Run()
	if err != nil {
		log.Fatalf("failed to start API: %v", err)
	}

}
